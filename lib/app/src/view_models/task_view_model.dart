import 'package:exemplomvvm/app/src/models/task_model.dart';
import 'package:exemplomvvm/app/src/models/task_repository.dart';
import 'package:flutter/material.dart';

class TaskViewModel extends ChangeNotifier {
  final TaskRepository repository;

  TaskViewModel(this.repository);

  void addTask(TaskModel task) {
    repository.create(task); //
    notifyListeners();
  }

  void removeTask(TaskModel task) {
    repository.delete(task);
    notifyListeners();
  }

  void editTask(TaskModel task, String title, String description) {
    repository.update(task, title, description);
    notifyListeners();
  }

  List<TaskModel> get tasks => repository.read();
}
