import 'dart:convert';
import 'package:exemplomvvm/app/src/models/task_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class TaskRepository {
  void create(TaskModel task);
  List<TaskModel> read();
  void update(TaskModel task, String title, String description);
  void delete(TaskModel task);
}

class TaskInMemoryRepository implements TaskRepository {
  final _tasks = <TaskModel>[];

  @override
  void create(TaskModel task) {
    _tasks.add(task);
  }

  @override
  void delete(TaskModel task) {
    _tasks.removeWhere((e) => e == task);
  }

  @override
  List<TaskModel> read() => _tasks;

  @override
  void update(TaskModel task, String title, String description) {
    task.title = title;
    task.description = description;
  }
}

class TaskSharedPreferencesRepository implements TaskRepository {
  final String _key = 'DB1';
  SharedPreferences? _sharedPreferences;

  TaskSharedPreferencesRepository() {
    _initializeSharedPreferences();
  }

  void _initializeSharedPreferences() async {
    try {
      _sharedPreferences = await SharedPreferences.getInstance();
    } catch (e) {
      SharedPreferences.setMockInitialValues({});
      _sharedPreferences = await SharedPreferences.getInstance();
    }
  }

  @override
  void create(TaskModel task) {
    final _modelTasks = read();
    final _stringfiedTasks = <String>[];
    _modelTasks.add(task);
    for (final _task in _modelTasks) {
      _stringfiedTasks.add(_task.toJson());
    }
    _sharedPreferences!.setStringList(_key, _stringfiedTasks);
  }

  @override
  void delete(TaskModel task) {
    final _modelTasks = read();
    final _stringfiedTasks = <String>[];
    _modelTasks.removeWhere((t) {
      return t.id == task.id;
    });
    for (final _task in _modelTasks) {
      _stringfiedTasks.add(_task.toJson());
    }
    _sharedPreferences!.setStringList(_key, _stringfiedTasks);
  }

  @override
  List<TaskModel> read() {
    final _tasks = <TaskModel>[];
    _sharedPreferences == null
        ? SharedPreferences.setMockInitialValues({})
        : null;
    final _db = _sharedPreferences?.getStringList(_key);
    if (_db != null) {
      for (final taskJson in _db) {
        _tasks.add(TaskModel.fromJson(json.decode(taskJson)));
      }
    }
    return _tasks;
  }

  @override
  void update(TaskModel task, String title, String description) async {
    List<TaskModel> list = await read();
    task.title = title;
    task.description = description;
    list.add(task);
    List<String> taskList = [];
    for (var item in list) {
      taskList.add(item.toJson());
    }
    _sharedPreferences!.setStringList(_key, taskList);
  }
}
