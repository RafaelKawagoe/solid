import 'dart:convert' show jsonEncode;

class TaskModel {
  String description;
  String title;
  int id;

  TaskModel(
    this.description,
    this.title,
    this.id,
  );

  String toJson() {
    final _map = {
      'description': description,
      'title': title,
      'id': id,
    };
    return jsonEncode(_map);
  }

  factory TaskModel.fromJson(Map json) {
    return TaskModel(json["description"], json["title"], json["id"]);
  }
}
