import 'package:flutter/material.dart';

import 'app/src/models/task_model.dart';
import 'app/src/models/task_repository.dart';
import 'app/src/view_models/task_view_model.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final TaskViewModel viewModel =
      TaskViewModel(TaskSharedPreferencesRepository());
  final titleController = TextEditingController();
  final descriptionController = TextEditingController();
  var flag = false;

  void addTask(TaskModel task) {
    viewModel.addTask(task);
    clear();
  }

  void clear() {
    titleController.clear();
    descriptionController.clear();
  }

  void dialogWidget(TaskModel task) {
    titleController.text = task.title;
    descriptionController.text = task.description;
    var popupTitle = "";
    flag ? popupTitle = "Edite a tarefa" : popupTitle = "Adicione a tarefa";
    showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
                title: Text(popupTitle),
                content: Column(children: [
                  TextField(
                    decoration: const InputDecoration(labelText: 'Titulo'),
                    controller: titleController,
                  ),
                  TextField(
                    decoration: const InputDecoration(labelText: 'Descrição'),
                    controller: descriptionController,
                  )
                ]),
                actions: <Widget>[
                  TextButton(
                      onPressed: () {
                        clear();
                        Navigator.pop(context, 'Cancel');
                      },
                      child: const Text("Cancelar")),
                  TextButton(
                      onPressed: () {
                        flag
                            ? viewModel.editTask(task, titleController.text,
                                descriptionController.text)
                            : addTask(TaskModel(
                                descriptionController.text,
                                titleController.text,
                                DateTime.now().millisecond));
                        flag = false;
                        Navigator.pop(context, 'Ok');
                      },
                      child: const Text("Confirmar"))
                ])).then((value) => clear());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            //deveria usar containers para margens?
            height: 20,
          ),
          const Text(
            "To do List",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
          AnimatedBuilder(
            animation: viewModel,
            // child: const Text('Lista vazia'),
            builder: (BuildContext context, Widget? child) {
              final _tasks = viewModel.repository.read();
              return Expanded(
                child: ListView.builder(
                  // itemCount: viewModel.tasks.length,
                  itemCount: _tasks.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              flag = true;
                              dialogWidget(_tasks[index]);
                            },
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    _tasks[index].title,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 12),
                                  ),
                                  Text(
                                    _tasks[index].description,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20),
                                  ),
                                ]),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                width: 5,
                              ),
                              ElevatedButton(
                                child: const Icon(Icons.remove),
                                onPressed: () {
                                  viewModel.removeTask(_tasks[index]);
                                },
                              ),
                            ],
                          )
                        ],
                      ),
                    );
                  },
                ),
              );
            },
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
          tooltip: 'Adicionar tarefa',
          child: const Icon(Icons.add),
          onPressed: () {
            flag = false;
            final task = TaskModel(descriptionController.text,
                titleController.text, DateTime.now().millisecondsSinceEpoch);
            dialogWidget(task);
          }),
    );
  }
}
